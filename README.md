# Blog

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

## Docker

### Preparar a aplicação para o release

```
 mix deps.get && mix deps.get --only prod
 npm install --prefix ./assets
 npm run deploy --prefix ./assets
 mix phx.digest
 source .env
 MIX_ENV=prod mix release
 ```

Startar a aplicação

- configurar o dev.exs com o seu banco de dados postgres

Startar a aplicação com o docker-compose

- precisa configurar o `dev.exs>hostname` para **db**


 Startar a aplicação em ambiente docker de produção
 Obrigatório passar com variáveis de ambiente: SECRET_KEY_BASE, DATABASE_URL

 ```
               # nomerepo/nomeprojeto:versao .
 docker build -t romenigld/blog_romenig .
                                       # nomerepo/nomeprojeto:versao
 docker run --env-file .env -p 8083:4000 romenigld/blog_romenig
```

Now you can visit [`localhost:8083`](http://localhost:8083) from your browser.

For run a migrate run the docker:
```
docker run --env-file .env -p 8083:4000 romenigld/blog_prod
```

open new terminal taband run
```
 docker ps
 docker exec -it <Container_ID> sh
 ls
 cd bin
./blog eval "Blog.Release.migrate"
```

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
